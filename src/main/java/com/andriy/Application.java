package com.andriy;

import com.andriy.people.Driver;
import com.andriy.people.Pasenger;
import com.andriy.transport.Bas;
import com.andriy.transport.Car;
import org.apache.logging.log4j.*;

import java.io.IOException;

public class Application {

    private static Logger logger1 = LogManager.getLogger(Application.class);

    public static void main(String[] args) {

        Pasenger pasenger = new Pasenger("Kolya", "Pole", 25, "manager",  50);
        pasenger.generateMessage();

        logger1.error("Exception", new IOException("HI"));

        logger1.trace("This is a trace manager");
        logger1.debug("This is a debug manager");
        logger1.info("This is a info manager");
        logger1.warn("This is a warn manager");
        logger1.error("This is a error manager");
        logger1.fatal("This is a fatal manager");

        Driver driver = new Driver("Andriy", "Tryndey", 40, "driver", 'B', 5);
        Bas bas1 = new Bas("Ikarus", 70);
        Car note = new Car("Japan", "note", "dark", 120);
        Pasenger pasenger1 = new Pasenger("Ivan", "Pupkin", 35, "teacher", 50);

        for (int i = 0; i < 2; i++){
            driver.generateMessage();
            bas1.generateMessage();
        }

    }
}
