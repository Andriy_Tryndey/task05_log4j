package com.andriy.people;

public class Driver extends Person {
    private char category;
    private int experience;

    public Driver(String name, String surname, int age, String profesion, char category, int experience) {
        super(name, surname, age, profesion);
        this.category = category;
        this.experience = experience;
    }

    public char getCategory() {
        return category;
    }

    public Driver setCategory(char category) {
        this.category = category;
        return this;
    }

    public int getExperience() {
        return experience;
    }

    public Driver setExperience(int experience) {
        this.experience = experience;
        return this;
    }

    @Override
    public String toString() {
        return "Driver{" + super.toString() +
                "category=" + category +
                ", experience=" + experience +
                '}';
    }

    public void generateMessage() {
        System.out.println("I am driver");
    }
}
