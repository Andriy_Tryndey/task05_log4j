package com.andriy.people;

public class Person {
    private String name;
    private String surname;
    private int age;
    private String profesion;

    public Person(String name, String surname, int age, String profesion) {
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.profesion = profesion;
    }

    public String getName() {
        return name;
    }

    public Person setName(String name) {
        this.name = name;
        return this;
    }

    public String getSurname() {
        return surname;
    }

    public Person setSurname(String surname) {
        this.surname = surname;
        return this;
    }

    public int getAge() {
        return age;
    }

    public Person setAge(int age) {
        this.age = age;
        return this;
    }

    public String getProfesion() {
        return profesion;
    }

    public Person setProfesion(String profesion) {
        this.profesion = profesion;
        return this;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", age=" + age +
                ", profesion='" + profesion + '\'' +
                '}';
    }
}
