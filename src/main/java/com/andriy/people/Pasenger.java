package com.andriy.people;

public class Pasenger extends Person {
    private int weightOfSuitcases;

    public Pasenger(String name, String surname, int age, String profesion, int weightOfSuitcases) {
        super(name, surname, age, profesion);
        this.weightOfSuitcases = weightOfSuitcases;
    }

    public int getWeightOfSuitcases() {
        return weightOfSuitcases;
    }

    public Pasenger setWeightOfSuitcases(int weightOfSuitcases) {
        this.weightOfSuitcases = weightOfSuitcases;
        return this;
    }

    @Override
    public String toString() {
        return "Pasenger{" + super.toString() +
                "weightOfSuitcases=" + weightOfSuitcases +
                '}';
    }

    public void generateMessage() {
        System.out.println("I am pasenger");
    }
}
