package com.andriy.transport;

public class Car {
    private String producer;
    private String model;
    private String color;
    private int speed;

    public Car(String producer, String model, String color, int speed) {
        this.producer = producer;
        this.model = model;
        this.color = color;
        this.speed = speed;
    }

    public String getProducer() {
        return producer;
    }

    public Car setProducer(String producer) {
        this.producer = producer;
        return this;
    }

    public String getModel() {
        return model;
    }

    public Car setModel(String model) {
        this.model = model;
        return this;
    }

    public String getColor() {
        return color;
    }

    public Car setColor(String color) {
        this.color = color;
        return this;
    }

    public int getSpeed() {
        return speed;
    }

    public Car setSpeed(int speed) {
        this.speed = speed;
        return this;
    }

    @Override
    public String toString() {
        return "Car{" +
                "producer='" + producer + '\'' +
                ", model='" + model + '\'' +
                ", color='" + color + '\'' +
                ", speed=" + speed +
                '}';
    }
}
