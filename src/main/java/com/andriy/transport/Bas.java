package com.andriy.transport;

public class Bas {
    private String model;
    private int passengerCapacity;

    public Bas(String model, int passengerCapacity) {
        this.model = model;
        this.passengerCapacity = passengerCapacity;
    }

    public String getModel() {
        return model;
    }

    public Bas setModel(String model) {
        this.model = model;
        return this;
    }

    public int getPassengerCapacity() {
        return passengerCapacity;
    }

    public Bas setPassengerCapacity(int passengerCapacity) {
        this.passengerCapacity = passengerCapacity;
        return this;
    }

    @Override
    public String toString() {
        return "Bas{" +
                "model='" + model + '\'' +
                ", passengerCapacity=" + passengerCapacity +
                '}';
    }

    public void generateMessage() {
        System.out.println("It's a bas");
    }
}
